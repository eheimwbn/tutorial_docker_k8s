
![MIT](https://img.shields.io/apm/l/gitlab?style=plastic)  
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)

### Work in progress :)

# Tutorial
## docker step one
* Erstellen eines Python Webservers der in Docker arbeitet
* Verstehen eines Dockerfiles
* Ändern eines Dockerfiles
* Image aus einem Dockerfile bauen
* Starten eines Containers aus einem Dockerfile

## linux step one
* Verstehen einiger Grundlegender Linux CLI Befehle


#### mirrored on ekspe.git
