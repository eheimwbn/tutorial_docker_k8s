#!/usr/bin/env python3
# This is a sample Python script. To demonstrate a simple Python Webserver running in a docker container as part of a
# beginner tutorial.

"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
from datetime import datetime

# global variables
get_count = 0
my_time = datetime.now()

class S(BaseHTTPRequestHandler):
    """create a simple WS witch responds with a name, a created time the count how often he was queried """

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        """uses globel variables to get the created time and the instance query counter for get requests"""
        global get_count
        global my_time
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {} <br>".format(get_count).encode('utf-8'))
        self.wfile.write("I was started at {} <br>".format(my_time.strftime("%d.%m.%Y, %H:%M:%S")).encode('utf-8'))
        self.wfile.write("My name is  {} <br>".format(my_time.strftime("%S_%f")).encode('utf-8'))
        get_count += 1
        logging.info("request count: " + str(get_count))


def run(server_class=HTTPServer, handler_class=S, port=8080):
    """create an class instance and run it and wire logs to stdout (for dockers sake)"""
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    logging.info(server_address)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    run()
