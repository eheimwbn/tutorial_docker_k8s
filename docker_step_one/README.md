# Step one

## Abhänigkeiten:
* git
* Docker
* Python3 (Dockerimage reicht)
* Zugriff auf das python_latest image

## Erklärung:
Der python file unter app/server.py erstellt einen einfachen Webserver.  
Der Server antwortet mit seinem Namen, der Uhrzeit wann er erstellt wurde und  
einem Counter, wie oft er seit seinem Start angesprochen wurde. 

## Aufgaben:
01. verstehe den Dockerfile 
02. baue aus dem File ein Image
03. erstelle aus dem Image einen Container
04. über prüfe, ob dein container läuft und er dir antwortet
05. beantworte die Fragen:
   * wie vergibst du den port im Container
   * wie den Port auf dem Host auf dem dein Container läuft
   * was macht das „-d" im docker run
   * was das '--rm'
06. springe in den container   
07. ändere den port im dockerfile
08. nutze ein anders base image
09. starte den container mehrmals
10. lese die los des containers währende er läuft und du in abfragst


## Kommandos zu nutzen/auszuprobieren:
```
docker run -d --rm -p 8080:8080/tcp tutorial  
docker build -t tutorial_step_one:latest .
docker images
docker rmi $(docker images -q)
docker rm $(docker ps -aq)
docker run tutorial_step_one
docker ps
docker ps -a
docker ps -aq
docker kill <id>
docker logs <container name>
docker logs -f <container name>
docker exec -it <container name> /bin/sh
```

### Python?
ß1. Ändere die Antwort des servers auf GET anfahren
   * ändere den Namen den er zurückgibt
   * lass ihn zusätzlich ausgeben um welche Uhrzeit er geantwortet 
   

#### Todo
for i in {1..5}; do curl -s localhost:8080 | grep -Po "for \d+" ;done
--cpus="1.5"
   
